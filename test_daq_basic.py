"""DAQ functionality As a developer, I want to be able to configure the DAQ So that we can send different types of data through feature tests."""

from pytest_bdd import (
    given,
    scenario,
    then,
    when,
)


@scenario('daq_basic_functionality.feature', 'Configuring the DAQ to channelised data')
def test_configuring_the_daq_to_channelised_data():
    """Configuring the DAQ to channelised data."""


@scenario('daq_basic_functionality.feature', 'Configuring the DAQ to raw data')
def test_configuring_the_daq_to_raw_data():
    """Configuring the DAQ to raw data."""


@scenario('daq_basic_functionality.feature', 'Turning the DAQ off')
def test_turning_the_daq_off():
    """Turning the DAQ off."""


@scenario('daq_basic_functionality.feature', 'Turning the DAQ on')
def test_turning_the_daq_on():
    """Turning the DAQ on."""


@given('the DAQ is available')
def _():
    """the DAQ is available."""
    raise NotImplementedError


@given('the DAQ is in adminMode OFFLINE')
def _():
    """the DAQ is in adminMode OFFLINE."""
    raise NotImplementedError


@given('the DAQ is in adminMode ONLINE')
def _():
    """the DAQ is in adminMode ONLINE."""
    raise NotImplementedError


@given('the DAQ is in health state OK')
def _():
    """the DAQ is in health state OK."""
    raise NotImplementedError


@given('the DAQ is in health state UNKNOWN')
def _():
    """the DAQ is in health state UNKNOWN."""
    raise NotImplementedError


@given('the DAQ is in the DISABLE state')
def _():
    """the DAQ is in the DISABLE state."""
    raise NotImplementedError


@given('the DAQ is in the ON state')
def _():
    """the DAQ is in the ON state."""
    raise NotImplementedError


@when('I send the Start command with channelised data')
def _():
    """I send the Start command with channelised data."""
    raise NotImplementedError


@when('I send the Start command with raw data')
def _():
    """I send the Start command with raw data."""
    raise NotImplementedError


@when('I set adminMode to OFFLINE')
def _():
    """I set adminMode to OFFLINE."""
    raise NotImplementedError


@when('I set adminMode to ONLINE')
def _():
    """I set adminMode to ONLINE."""
    raise NotImplementedError


@then('the DAQ is in channelised data mode')
def _():
    """the DAQ is in channelised data mode."""
    raise NotImplementedError


@then('the DAQ is in raw data mode')
def _():
    """the DAQ is in raw data mode."""
    raise NotImplementedError

