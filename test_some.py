"""Testing Metadata Recording in DAQ Output feature tests."""

from pytest_bdd import (
    given,
    scenario,
    then,
    when,
)


@scenario('daq_metadata.feature', 'Daq receiver configuration is passed through subarray and station')
def test_daq_receiver_configuration_is_passed_through_subarray_and_station():
    """Daq receiver configuration is passed through subarray and station."""


@scenario('daq_metadata.feature', 'Daq recorded data contains configuration')
def test_daq_recorded_data_contains_configuration():
    """Daq recorded data contains configuration."""


@scenario('daq_metadata.feature', 'Metadata is recorded as expected in the output file')
def test_metadata_is_recorded_as_expected_in_the_output_file():
    """Metadata is recorded as expected in the output file."""


@scenario('daq_metadata.feature', 'Missing metadata in the observation')
def test_missing_metadata_in_the_observation():
    """Missing metadata in the observation."""


@given('DAQ has been configured with the following metadata:
| Metadata Key    | Metadata Value              |
| Observation ID  | OBS-123456                  |')
def _():
    """DAQ has been configured with the following metadata:
| Metadata Key    | Metadata Value              |
| Observation ID  | OBS-123456                  |."""
    raise NotImplementedError


@given('DAQ has not been configured with additional metadata')
def _():
    """DAQ has not been configured with additional metadata."""
    raise NotImplementedError


@given('DAQ is collecting data')
def _():
    """DAQ is collecting data."""
    raise NotImplementedError


@given('DAQ is ready to be configured')
def _():
    """DAQ is ready to be configured."""
    raise NotImplementedError


@given('a Station is ready to be configured')
def _():
    """a Station is ready to be configured."""
    raise NotImplementedError


@given('a Subarray is ready to be configured')
def _():
    """a Subarray is ready to be configured."""
    raise NotImplementedError


@given('the DAQ configuration contains the same unique ID')
def _():
    """the DAQ configuration contains the same unique ID."""
    raise NotImplementedError


@given('the DAQ system is running')
def _():
    """the DAQ system is running."""
    raise NotImplementedError


@given('the Station configuration contains the same unique ID')
def _():
    """the Station configuration contains the same unique ID."""
    raise NotImplementedError


@given('the Subarray configuration contains a unique ID')
def _():
    """the Subarray configuration contains a unique ID."""
    raise NotImplementedError


@given('the Subarray is configured')
def _():
    """the Subarray is configured."""
    raise NotImplementedError


@when('DAQ records data to file')
def _():
    """DAQ records data to file."""
    raise NotImplementedError


@when('data is saved to an output file')
def _():
    """data is saved to an output file."""
    raise NotImplementedError


@then('the DAQ configuration will contain the same unique ID')
def _():
    """the DAQ configuration will contain the same unique ID."""
    raise NotImplementedError


@then('the Station configuration will contain the same unique ID')
def _():
    """the Station configuration will contain the same unique ID."""
    raise NotImplementedError


@then('the Subarray configuration will contain a unique ID')
def _():
    """the Subarray configuration will contain a unique ID."""
    raise NotImplementedError


@then('the output file should contain default values for missing metadata:
| Metadata Key    | Metadata Value              |
| Observation ID  | Default_Observation_ID      |')
def _():
    """the output file should contain default values for missing metadata:
| Metadata Key    | Metadata Value              |
| Observation ID  | Default_Observation_ID      |."""
    raise NotImplementedError


@then('the output file should contain the recorded metadata:
| Metadata Key    | Metadata Value              |
| Observation ID  | OBS-123456                  |')
def _():
    """the output file should contain the recorded metadata:
| Metadata Key    | Metadata Value              |
| Observation ID  | OBS-123456                  |."""
    raise NotImplementedError


@then('the same unique ID will be stored to the file\'s metadata')
def _():
    """the same unique ID will be stored to the file's metadata."""
    raise NotImplementedError

