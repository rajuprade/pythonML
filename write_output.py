def write_dict_to_file(output_file_path, data_dict):
    with open(output_file_path, "w") as file:
        for key, value in data_dict.items():
            file.write(f"{key}: {value}\n")

# Example dictionary
data_dict = {
    "Observation ID": "OBS-123456",
    "Temperature": "25°C",
    "Pressure": "1013 hPa"
}

# Output file path
output_file_path = "output.txt"

# Write the dictionary to the output file
write_dict_to_file(output_file_path, data_dict)

